<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Groupe de routes qui s'occupera uniquement des controlleurs pour les vues admin (Nommer comme suit : 'Definition'_AdminController)
Route::group(['prefix' => '/admin', 'middleware' => 'auth.admin'], function(){
        Route::get('/', 'AdminController@index');

        Route::resource('/users','UsersController');
        Route::get( '/users/edit/{id}', 'UsersController@edit');
        Route::get( '/users/create', 'UsersController@create');
        Route::get('/news/delete/{id}', 'AdminArticlesController@destroy');

        Route::resource('/news','AdminArticlesController');
        Route::get('/news/delete/{id}', 'AdminArticlesController@destroy');
        Route::get( '/users/create', 'AdminArticlesController@create');
});


// Route pour l'auth
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
// Route pour l'enregistrement
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
//END Auth


// Groupe de routes qui s'occupera que de la redirection des controlleurs pour les vues racines. (Nommer comme suit : 'Definition'_RacineController)
Route::group(['prefix' => "/"], function () {

    // Route du root
    Route::get('/', function(){
        return view("pages.index");
    });

    // Route pour la partie Articles du site.
    Route::resource('article', 'ArticlesController');

    // Route par défault pour critiques
    Route::get('critiques', function(){
        return view("pages.critiques.index_critiques");
    });

    // Route par défault pour dossiers
    Route::get('dossiers', function(){
        return view("pages.dossiers.show_dossier");
    });

    // Route par défault pour jeux
    Route::get('jeux', function(){
        return view("pages.jeux.index_jeux");
    });
    // Route par défault pour chroniques
    Route::get('chroniques', function(){
        return view("pages.chroniques.index_chroniques");
    });

    // Route par défault pour les profils
    Route::get('mon-profil', function(){
        return view("pages.profil.monprofil");
    });

    // Route par défault pour videos
    Route::get('videos', function(){
        return view("pages.videos.index_videos");
    });

    //BEGIN Auth
        // Route pour les connexions / enregistrements
    Route::get("connexion", function(){
        return view("modal.connection_modal");
    });
    Route::get('sinscrire', function(){
        return view("modal.register_modal");
    });


    //BEGIN Faq
    Route::get('faq', function(){
        return view ("pages.other_pages.faq");
    });

    Route::get('redaction', function(){
        return view ("pages.other_pages.redaction");
    });

});


// Groupe de routes pour les forums
Route::group(['prefix' => "/forums"], function () {
    Route::get('/', function(){
        return view ("pages.forums.index");
    });
});