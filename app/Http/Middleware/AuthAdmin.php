<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class AuthAdmin
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() == null) {
            return redirect()->guest('/connexion')->with("erreur", "Veuillez vous connecter.");
        } else if (Auth::user()->level == 0) {
            return redirect()->back()->with('erreur', "Vous n'avez pas l'autorisation devenir ici.");
        }
        return $next($request);
    }
}
