<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LinksController extends Controller
{
    public function show() {
        $link = Link::findOrFail($id);
        return new RedirectResponse($link->url);
    }
    public function create(){
        return view('links.create');
    }

    public function store(){
        $link = Link::create(['url' => Input::get('url')]);
        return view('links.success', compact('link'));
        dd(\Input::get('url'));
    }
}
