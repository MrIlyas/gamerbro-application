<!-- NAV Only -->
@include("modal.modal_connection_type")
<nav class="barre navbar navbar-inverse">
        <!-- premiere barre-->
        <div class="barre container">
            <div id="navbar" class="navbar-default2">
                <!-- Welcome + menu xs -->
                <ul class="nav navbar-nav barre-principale">
                    <li class="nav-logo"><a href="/"><img src="/img/assets/logo/logo.png" alt="Gamerbro.fr" /></a></li>
                    @if(Auth::check())
                       <li><img src="/img/member-profil/id1.jpg" class="member-info-profil-img" /></li>
                        <li><a href="#" dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> {{ Auth::user()->name }}     <span class="caret"></span></a>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="/mon-profil">Configurer son profil</a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#">Something else here</a></li>
                            @if(Auth::user()->level > 0)
                                <li role="separator" class="divider"></li>
                                <li><a href="/admin">Admin</a></li>
                            @endif
                            </ul>
                        <li class="before-search"><a href="/auth/logout">Déconnexion</a></li>
                    @else
                        <li><a href="#" data-toggle="modal" data-target="#myModal">Se connecter</a></li>
                        <li class="before-search"><a href="/sinscrire">S'inscrire</a></li>
                    @endif
                <!-- /Welcome + menu xs  -->
                <!-- Input Recherche -->
                <li id="form-menu">
                    <form action="">
                        <input class="form-control search  navbar-right" id="disabledInput" type="text" placeholder="Bientot une recherche ici ...">
                    </form>
                </li>
                <!-- /Input Recherche -->
                </ul>
            </div>
        </div>
        <!-- /premiere barre-->

        <!-- barre menu -->
        <div class="container barre-menu" data-spy="affix" data-offset-top="100" id="nav" style="z-index: 9999;" >
            <div id="navbar" class="navbar-default">
                <a class="hidden-lg hidden-md hidden-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">      Menu
                    Menu
                </a>
                <ul class="dropdown-menu hidden-lg hidden-md hidden-sm" aria-labelledby="dropdownMenu1">
                    @include("templates.menu_list")
                </ul>
                <ul class="nav navbar-nav hidden-xs">
                    @include("templates.menu_list")
                </ul>
         <!--/.nav-collapse -->
        </div>
    </div>
        <!-- /barre menu -->
    </nav>
<!-- /NAV Only -->