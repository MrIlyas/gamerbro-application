<!--
██████╗  ██████╗ ██╗    ██╗███████╗██████╗ ███████╗██████╗     ██████╗ ██╗   ██╗
██╔══██╗██╔═══██╗██║    ██║██╔════╝██╔══██╗██╔════╝██╔══██╗    ██╔══██╗╚██╗ ██╔╝
██████╔╝██║   ██║██║ █╗ ██║█████╗  ██████╔╝█████╗  ██║  ██║    ██████╔╝ ╚████╔╝
██╔═══╝ ██║   ██║██║███╗██║██╔══╝  ██╔══██╗██╔══╝  ██║  ██║    ██╔══██╗  ╚██╔╝
██║     ╚██████╔╝╚███╔███╔╝███████╗██║  ██║███████╗██████╔╝    ██████╔╝   ██║
╚═╝      ╚═════╝  ╚══╝╚══╝ ╚══════╝╚═╝  ╚═╝╚══════╝╚═════╝     ╚═════╝    ╚═╝

 ██████╗  █████╗ ███╗   ███╗███████╗██████╗ ██████╗ ██████╗  ██████╗    ███████╗██████╗
██╔════╝ ██╔══██╗████╗ ████║██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔═══██╗   ██╔════╝██╔══██╗
██║  ███╗███████║██╔████╔██║█████╗  ██████╔╝██████╔╝██████╔╝██║   ██║   █████╗  ██████╔╝
██║   ██║██╔══██║██║╚██╔╝██║██╔══╝  ██╔══██╗██╔══██╗██╔══██╗██║   ██║   ██╔══╝  ██╔══██╗
╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗██║  ██║██████╔╝██║  ██║╚██████╔╝██╗██║     ██║  ██║
 ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝╚═╝     ╚═╝  ╚═╝
-->
<html lang="fr">

    <head>
        @include("templates.head_template")
    </head>

    <body @yield("body")>

        @include("templates.menu_template")
        @yield("dossier-top")
        <div class="container-fluid contenu">
            <!-- Erreur -->

            @if(!empty(session('succes')))
                <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <i class="fa fa-info"></i> {{ session('succes') }}
                </div>
            @elseif(!empty(session('erreur')))
                <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <i class="fa fa-info"></i> {{ session('erreur') }}
                </div>
            @endif

           <!-- /Erreur -->
            @yield("content")

        </div>



        @include("templates.footer")

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/app.js"></script>

        <!--TEST-->
        <script src="js/classie.js"></script>
        <script src="js/main.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->


    </body>
</html>