@extends("templates.template")
@section("add_custom_css") <link href="/css/modal_connexion.css" rel="stylesheet" />@endsection
@section("content")

<div class="connexion col-lg-12 col-xs-12 col-md-12 col-sm-12">
    <div class="form-connexion col-lg-9 col-xs-9 col-md-9 col-sm-9">
            <h1>Se connecter sur GamerBro.fr</h1>
                @if(!empty($errors->all()))
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4>Erreur : </h4>
                    <p>
                        @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                    </p>
                </div>
            @endif
                <form class="form col-md-center-block" method="POST" action="auth/login">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" name="email" placeholder="Votre email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password" id="password" placeholder="Votre mot de passe">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Se connecter</button>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember">
                                Se souvenir de moi
                            </label>
                        </div>

                    </div>
               </form>
    </div>

    <div class="form-menu col-lg-3 col-xs-3 col-md-3 col-sm-3">
        <span>
            <a href="sinscrire">S'inscrire</a>
        </span>
        <span>
            <a href="faq">Besoin d'aide ?</a>
        </span>
    </div>
</div>

    @endsection