@extends("templates.template")
@section("add_custom_css")
    <link href="/css/modal_connexion.css" rel="stylesheet">
@endsection

@section("title") @endsection
@section("content")
    <!-- resources/views/auth/register.blade.php -->

    <div class="connexion col-lg-12 col-xs-12 col-md-12 col-sm-12">
        @if(!empty($errors->all()))
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4>Erreur : </h4>
            <p>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </p>
        </div>
        @endif
        <div class="form-connexion col-lg-9 col-xs-12 col-md-9 col-sm-12">
            <h1>S'enregistrer</h1>
            <form class="form col-md-center-block" method="POST" action="auth/register">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" class="form-control input-lg" name="name" placeholder="Votre pseudonyme" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control input-lg" name="email" placeholder="Votre email" value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control input-lg" name="password"  placeholder="Votre mot de passe">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control input-lg" name="password_confirmation"  placeholder="Confirmez votre mot de passe">
                        <div class="form-group">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">S'enregistrer</button>
                </div>
            </div>
            </form>
        </div>

        <div class="form-menu col-lg-3 col-xs-12 col-md-3 col-sm-12">
            <span>
                <a href="connexion">Se connecter</a>
            </span>
            <span>
                <a href="faq">Besoin d'aide ?</a>
            </span>
        </div>
    </div>

@endsection