<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4>Oh snap! You got an error!</h4>
                        <p>
                 @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                 @endforeach
                        </p>
                    </div>
                @endif
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:red;"><span aria-hidden="true">&times;</span></button>
                <h4 style="text-transform: uppercase;">Se connecter sur GamerBro.fr</h4>
            </div>
            <form class="form col-md-center-block" method="POST" action="auth/login">
            <div class="modal-body">


                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" name="email" placeholder="Votre email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password" id="password" placeholder="Votre mot de passe">
                    </div>
                    <div class="form-group">

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember">
                                Se souvenir de moi
                            </label>
                        </div>

                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Se connecter</button>
                <a class="btn btn-block btn-social btn-facebook"><i class="fa fa-facebook"></i>
                    Sign in with Facebook</a>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->