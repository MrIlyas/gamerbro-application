
@extends("templates.template")
@section("add_custom_css")
    <link href="/css/index_article.css" rel="stylesheet">
@endsection

@section("title") - Liste d'articles @endsection
@section("content")
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="index">

            <h1>Liste des articles</h1>
            {{--Initialisation du compteur et le cacher du public via une astuce vu sur Stack. */ $i=0;/* --}}
            @foreach($articles as $article)
                {{--Incrementation du compteur*/ $i++;/* --}}
                @if($i % 2)
            <div class="ct img-right">
                @else
            <div class="ct img-left">
                @endif
                <img src="/img/assets/article/1.jpg" alt=""/>
                <div class="content-text">{{ $article->content }}</div>
            </div>
            <div class="a-d">
                <div class="author">Par MrIlyas</div>
                <div class="datetime">{{ $article->created_at->diffForHumans() }}</div>
            </div>
            @endforeach

            <div class="pagination">
                <ul>
                        <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                        <li class="next"><a href="#" style="float: left;">Newer <span aria-hidden="true">&rarr;</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    @include("templates.sidebar")
@endsection
