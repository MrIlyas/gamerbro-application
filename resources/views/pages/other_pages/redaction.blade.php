<?php $users = \App\User::all() ?>

@extends("templates.template")

@section("title") La rédaction @endsection

@section("add_custom_css")
    <link href="/public/css/redaction.css" rel="stylesheet">
@endsection

@section("content")
    <div class="col-xs-12 col-md-12 col-lg-12" style="line-height: 2em; padding-bottom: 50px;">
        <div class="redaction col-lg-12">
            <h1>@yield("title")</h1>
            @foreach($users as $user)
                @if($user->level >= 1)
                    <div class="col-lg-2">
                        <div class="personne">
                            Pseudo : {{ $user->name }}<br />
                            <em>"Je suis un machin"</em>
                            <p>Cool !</p>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="recrutement">
            <h1>Recrutement</h1>
            <div>
                Afin de renforcer son équipe éditoriale, Gamerbro.fr recrute des personnes de plus de 17 ans, passionnées (l'expérience est un plus mais n'est pas un élément déterminant), motivées, et qui n'ont pas peur de challenges.
                <br /><br />
                Alors si vous êtes de ceux-là, vous êtes peut-être fait pour rejoindre les divers départements afin de grossir les rangs et renforcer la rédaction en place.
                <br /><br />
                Seule contrainte, être disponible quotidiennement pendant les 5 jours de la semaine.
                <br /><br />
            </div>
            <div>
                <strong>Rédacteurs :</strong>
                <br /><br />
                Vous avez de l'envie et de la passion à revendre pour le domaine vidéo-ludique et votre culture "Gamer" ne s’arrête pas forcément au plombier moustachu avec sa casquette rouge.
                Vous serez amenés à rédiger du contenu pour Gamerbro.fr (Actualités, Tests, Preview …)
                Vous avez l’esprit d’équipe et de synthèse, vous partagez vos informations avec vos pairs et la communication ne vous fait pas peur.
                Une petite formation est prévue en interne.
                Posséder une base en anglais.
                Vous êtes particulièrement motivé pour cela ? Envoyez-nous un mail avec votre présentation, vos motivations, pourquoi vous avez envie de rejoindre Gamerbro.fr.
            </div>
            <br /><br />
                <strong>Correcteur :</strong>
            <br /><br />
                Vous avez un orthographe irréprochable et corriger nos fautes ne vous énerve pas.
                Vous êtes disponible en journée
                Vous êtes particulièrement motivé pour cela ? Envoyez-nous un mail avec votre présentation, vos motivations et pourquoi vous avez envie de rejoindre Gamerbro.fr
            <div>
                <br /><br />
                <strong>Rédacteur Base de données :</strong>
                <br /><br />
                Vous êtes le cœur névralgique entre les rédacteurs et les utilisateurs et votre tâche consistera à modifier ou ajouter des éléments dans notre grande base de données
                Le travail répétitif ne vous fait pas peur.
                Vous êtes particulièrement motivé pour travailler au sein d'une immense base de données ? Envoyez-nous un mail avec votre présentation, vos motivations et pourquoi vous avez envie de rejoindre Gamerbro.fr
            </div>
            <div>
                <br /><br />
                <strong> Monteur/Cadreur vidéo :</strong>
                <br /><br />

                Vous êtes à l’aise avec des logiciels de montage.
                Vous travaillez de manière autonome et la communication ne vous fait pas peur.
                Habiter sur Bruxelles, Belgique (et alentours) ou Paris, France (ou dans le coin également) serait un gros plus.
                Vous êtes particulièrement motivé pour filmer Gamerbro.fr 24 heures sur 24, 7 jours sur 7 ? Envoyez-nous un mail avec vos motivations et pourquoi vous avez envie de rejoindre Gamerbro.fr
            </div>
            <div>
                <br /><br />
                <strong>Comment nous joindre ?</strong>
                <br /><br />
                Le seul recours est le mail : recrutement[at]Gamerbro.fr[dot]be avec comme sujet "Recrutement : votre pseudonyme" sur Gamerbro.fr, nous insistons sur le fait qu'au préalable, vous vous soyez inscrit sur le site.

                Sachez enfin que nous répondrons à tous les volontaires, que ça soit positivement ou négativement.
            </div>
        </div>
    </div>
@endsection