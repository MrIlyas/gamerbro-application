@extends("templates.template")
@section("add_custom_css")
    <link href="/css/show_article.css" rel="stylesheet">
    <link href="/css/show_dossier.css" rel="stylesheet">
@endsection

@section("title") $titre @endsection
@section("dossier-top")
    <div class="img-dossier">
        <div class="color-title-img"></div><img class="title-img" src="/img/dossier/1.jpg" /></div>
    </div>

    <div class="title-dossier">
        <h1>Dossier</h1>
        <span class="unknown">Metal Gear Solid : de la génèse jusqu'à la fin</span>
        <br />
        <span class="date">Le 1 septembre 2015</span>
        <h4>Par Ilyas "MrIlyas" Akhrif</h4>
    </div>

    <div class="menu menu-dossier">
        <span class="menu-title">Accès rapide</span>
        <ul>
            <li>
                test
            </li>
            <li>
                test
            </li>
            <li>
                test
            </li>
            <li>
                test
            </li>
            <li>
                test
            </li>
        </ul>
        <a href="#" class="totop">Vers le haut</a>
    </div>
@endsection


@section("content")
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <article>
            <div class="sharing">
                <a href="#" data-toggle="tooltip" data-placement="left" title="Partager sur Facebook ?"><img src="/img/assets/share/facebook.png"></a>  <a href="#" data-placement="right" title="Partager sur Twitter ?"><img src="/img/assets/share/twitter.png"></a>
            </div>

            <div class="text-justify">
                <p>Voici deux semaines environ, un titre faisait ses gros homonymes sur à peu près tous les sites disponibles sur les interwebs. La raison en était simple, mais laissait à quiconque joue un peu plus d'une heure par éon comme un goût de déjà-vu sur le palais et un arrière-goût de redondance plus étrange encore au fin fond de la gorge, juste à la berge du suc gastrique.&nbsp;</p>
                <p>Un jeu qui se moque des "pirates", ça n'a pas grand-chose de neuf. Je veux dire, c'est pas comme si bien avant cette histoire il y avait eu Batman: Arkham Asylum et ses histoires de héros qui se prend les pieds dans la cape pour s'écraser au sol comme un ancien mafieux pas trop digne de confiance au fin fond de l'East River. Il y a eu d'autres cas çà et là, mais on me pardonnera j'espère aisément de n'en citer aucun autre, Batman étant sans doute possible l'exemple le plus emblématique de ce retournement de situation aisément convertible en publicité peu chère et ô combien efficace.</p>
                <p>Ce n'est pas non plus comme si le piratage était un phénomène évitable - et loin de moi l'idée de faire pencher la balance d'un côté ou de l'autre quant à l'opportunisme de ses fervents adeptes ou l'utilité des DRM et moins encore l'éternel débat sur les chiffres de vente potentiels ou réels, le sujet est trop vaste et déborde sur nombre d'autres ; naïf celui qui pense détenir une vérité universelle.</p>
                <p>Non, rien de tout cela n'avait grand-chose de révolutionnaire, aussi ce qui retint mon attention fut plutôt le bout de code sur lequel portait cette publicité recherchée à défaut d'être osée. Il s'agissait en effet a priori d'un jeu dont la réputation n'est plus à faire. Permettez donc, puisqu'on parle de choses ludiques, que je vous aide façon Questions pour un Lampion : "je suis un jeu en 3D isométrique dans lequel le joueur gère un studio de création de jeux vidéo. Célèbre pour mes combinaisons de genres et univers de jeu à découvrir et débloquer, ma recherche de nouveaux concepts, mon apparence en pixel art, mes ventes records et mon addictivité, je suis... je suis..."</p>
                <p>Si vous avez répondu Game Dev Story, félicitations : vous avez mis le doigt sur le problème.</p>
                <div class="avis avis-right">
                    <div class="avatar">
                        <img src="/img/member-profil/id1.jpg">
                    </div>
                    <div class="avis-title">L'avis de <b>MrIlyas</b></div>
                    <div class="avis-subtitle">MORTEL ! </div>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores commodi consectetur corporis eveniet excepturi facilis fugiat fugit id illum necessitatibus numquam possimus reiciendis sit soluta sunt tempora veritatis, vero voluptate.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci, alias aperiam aspernatur aut cupiditate doloremque explicabo facere, illo iste magni natus perspiciatis placeat provident quasi quia repellat, vero. Odio!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aperiam cumque ex iure minima nostrum numquam odio, officia rerum tempore? Consectetur cumque ducimus eum, laudantium maiores mollitia reiciendis vero. Expedita?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dolorem obcaecati recusandae reiciendis veritatis? Eligendi mollitia odio perferendis quas quos sint vero? Adipisci ducimus est incidunt nam, soluta veniam voluptates!
                </div>
                <p>Game Dev Tycoon, à dire le vrai, ressemble plus à ce qu'aurait donné le célèbre jeu de Kairosoft s'il avait été développé par une bande de newbies sans grande expérience ou recherche sur l'ergonomie qu'au jeu culte sorti voici bientôt trois ans sur mobile.</p>

                <p style="text-align: center;"><img src="{{asset('img/1.jpg')}}" alt="" width="600"></p>

                <p style="text-align: center;">(<strong>Game Dev Story</strong>, version mobile)</p>

                <p>Et c'est bien là que le bât blesse : toute la presse en ligne a jeté son dévolu sur l'affaire, mais en occultant - volontairement ou pas - le côté plus cynique de ce coup de pub certes finement pensé mais non moins écœurant : Game Dev Tycoon est une immonde resucée à trois francs six sous.&nbsp;</p>
                <div class="quote quoteLeft">"JE CASSE TOUT"<br />- Jean-michel Jarre</div>
                <p>Tout dans le titre est repris de la machine à&nbsp;pomper le temps de Kairosoft : vue en 3D isométrique, système de points de recherche, dualité du développement entre contrats et nouveaux jeux, combinaisons à découvrir et fonctionnant plus ou moins bien, jeux de mots d'un goût douteux sur les noms des diverses bécanes, achats de licences pour le développement sur lesdites plates-formes, bugs à corriger avant les releases, entraînement du personnel, recrutement du personnel, passage d'un bureau minuscule/garage à la classe supérieure une fois un certain montant mis de côté, vannes pourries sur les noms des membres du personnel à recruter, choix des moyens donnés au recrutement, présence aux conventions avec un stand plus ou moins grand, même l'affichage de la date est repris de Game Dev Story.</p>
                <p>Certes, il y a quelques éléments modifiés, il serait d'une mauvaise foi crasse de le nier. Le problème, c'est qu'ils ont le plus souvent été modifiés sans la moindre réflexion dans le design. Ainsi, là où Game Dev Story débloquait les types et univers de jeu au fil des recrutement et entraînement des divers membres du staff et selon leurs compétences propres, Tycoon se contente de donner une liste lourde, indigeste, longue comme le bras à débloquer à l'envi et des membres du personnel modelables sans grand effort. De même, tandis que les développeurs de Story rentraient chez eux par eux-mêmes lorsque la fatique s'installait, Tycoon demande au joueur de cliquer dessus pour les envoyer en vacances. Vacances qui ne coûtent rien, ne rapportent rien, et dont le jeu se serait très franchement bien passé. Tout au plus peut-on se consoler en se disant que Greenheart ne pourra pas suivre la ligne du temps de Kairosoft, sans quoi leur second jeu serait quelque chose du type Hot Springs Tycoon. Et j'ai la vague impression que le public occidental n'est pas le plus friand qui soit lorsqu'il s'agit de simulateurs de stations thermales.</p>
                <p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="{{asset('img/2.jpg')}}" alt="" width="600">(<strong style="text-align: center;">Game Dev Tycoon</strong>)</p>
                <p>Mais alors d'où vient cet assourdissant silence autour de ce qu'il convient d'appeler un plagiat en bonne et dûe forme ? comme si le statut d'indépendant jouait le rôle de passe-droit, de carte "Sortez de prison", le simple fait d'être sans le sou permettrait donc de faire la nique aux principes de bienséance les plus élémentaires dans le petit monde de la propriété intellectuelle, de l'innovation et du design ?</p>
                <p>Permettez donc que cet humble scribouillard hausse le sourcil gauche devant l'incroyable différence de traitement dont font preuve la plupart de ses congénères électroniques : quand Zynga repompe l'idée et la substance du Tiny Tower de NimbleBit pour créer Dream Heights, tout le monde se fait un devoir de hurler au scandale ; quand Gamenauts calque son Ninja Fishing tout entier sur le Radical Fishing de Vlambeer, c'est l'apocalypse qui survient une fois de plus. Et ne parlons même pas des cas les plus amusants, tels que les accusations balancées au nez des créateurs d'Explodemon, qui auraient tout repris sur Splosion Man - sauf que le premier était en développement bien avant l'apparition du second. Mais je m'égare.</p>
                <p><img src="http://www.blogcdn.com/blog.games.com/media/2012/01/dream-heights-tiny-tower.jpg" alt="" width="600"></p>
                <p style="text-align: center;">(à gauche : <strong>Tiny Tower de NimbleBit</strong>&nbsp;- à droite : <strong>Dream Heights de Zynga</strong>)</p>
                <p>Quand Greenheart recopie faiblement tout un jeu culte de Kairosoft... Soudainement la foule s'égaille, la salle se vide, et seul un malheureux écho se propage tristement.</p>

                <p>Quel point commun trouver alors dans ces diverses situations ? Dans les premiers cas, la victime est "un pauvre indie sans le sou", le cliché le plus terrible et poignant qui soit dans le petit univers vidéoludique. Dans le dernier ? C'est un studio indépendant qui a fait ses preuves, qui continue contre vents et marées de rassasier le joueur mobile de sorties que je me permettrai d'appeler de qualité, raffinant à chaque itération ses concepts de base et dénichant dans les coins les plus absurdes les variations les plus inattendues, que ces tentatives de se renouveler soient couronnées de succès ou pas - on pensera notamment à leur essai de passage en free to play, qui s'est soldé par un retour fulgurant aux sources : version de démonstration et complète à acheter. Il faut croire que, de toutes les IAP possibles dans les deux jeux ayant tourné autour de la mythique manne de la micro-transaction, seules celles concernant les fonctionnalités permettant d'aboutir à une version "classique" de leurs jeux, à savoir sans publicité et avec rotation automatique, ont fait recette. Comble de l'ironie, le montant desdites transactions était identique à celui permettant d'acheter la version complète de leurs autres jeux. Mais je digresse. Gresse.</p>
                <p style="text-align: center;"><a href="/web/20130607113425/http://jasonleeelliott.com/wp-content/uploads/2011/09/NinjaVsRadical.jpg"><img src="{{asset('img/3.jpg')}}" alt="" width="370"></a></p>
                <p style="text-align: center;">&nbsp;(à gauche : <strong>Ninja Fishing</strong> - à droite : <strong>Radical Fishing)</strong></p>
                <p>Quelle différence alors ? Ce ne peut être le passif de l'agresseur, sans quoi Gamenauts ne se serait pas attiré les foudres de la presse. Ce n'est pas non plus une question de succès ou de moyens. Quelle que soit la manière dont je regarde la chose, la seule explication plausible - et encore - c'est l'éternel mythe du David indépendant contre le Goliath, qu'il soit lui aussi indépendant ou non. Et dans ce cas de figure... Oh hé, regardez : un tout petit indé débutant a copié le jeu du studio "énorme" qu'est Kairosoft. Trop cool non ?</p>
                <p>Non, pas cool. Même s'ils avaient copié Farmville à Zynga ou Fable à Lionhead, ce genre de pratiques est juste écœurant.</p>
                <p>Je n'ai rien contre l'univers "indie", que du contraire : je suis l'un des plus fervents supporters de la chose, et ce depuis un moment - qu'il s'agisse de soutien moral lorsque mes maigres moyens m'empêchent de traduire mon amour du jeu en monnaie sonnante et trébuchante ou bien de cas d'espèces. Et quand je dis un moment, c'était à vrai dire aux alentours de l'an de grâce 1998. On appelait la chose "shareware", et on trouvait des CD remplis de ces petites choses-là dans tous les coins. Entendons-nous bien également sur ce point : j'ai longtemps rêvé d'une hypothétique sortie d'un Game Dev Story remis au goût du jour et traduit en anglais sur PC. Mais pas sous cette forme.</p>
                <p>Quand l'hôpital se fout de la charité et se permet d'agiter sa sébile en plaidant le manque de moyens, je ne peux que ricaner sous cape devant l'abrutissante omerta dont bénéficie l'objet du délit. Car s'il s'agit bien ici de piratage, les victimes ne sont pas celles qu'on nous présente... et les sarcastiques profiteurs moins encore.</p><p></p></div>


            <footer>

                <div class="tags">
                    <span class="label">Liste des tags :</span><br />
                    <span class="label">Game Dev Story</span>
                    <span class="label">Game Dev Tycoon</span>
                    <span class="label">Tiny Tower</span>
                    <span class="label">Dream Heights</span>
                    <span class="label">NimbleBit</span>
                    <span class="label">Zynga</span>
                    <span class="label">Ninja Fishing</span>
                    <span class="label">Radical Fishing</span>
                    <span class="label">Gamenauts</span>
                    <span class="label">Kairosoft</span>
                    <span class="label">ios</span>
                    <span class="label">android</span>
                    <span class="label">pc</span>
                    <span class="label">et</span>
                    <span class="label">ipad</span>
                </div>

            </footer>
        </article>

    </div>
@endsection