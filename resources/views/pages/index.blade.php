@extends("templates.template")
@section("add_custom_css")
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.3.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style2.css" />
    <script src="js/modernizr.custom.js"></script>
@endsection

@section("title") @endsection

@section("content")
        {{ \Illuminate\Support\Str::slug("mon super article swag sluggifié via str.")}}
@endsection