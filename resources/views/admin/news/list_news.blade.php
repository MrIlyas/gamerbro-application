@extends("admin.templates.template_admin")

@section("content_admin")
<div class="table-responsive">
    <h1>Liste des utilisateurs de <strong>Gamerbro.fr</strong></h1><br />
    <div class="collapse" id="collapse">
            Mouais je ferais pas ca a ta place !
    </div>
    <a class="btn btn-primary col-lg-12 btn-lg" href="/admin/news/create">Ajouter un nouveau membre manuellement</a><br /><br /><br />
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>title</th>
            <th>slug</th>
            <th>subtitle</th>
            <th width="300px;">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($articles as $article)
            <tr>
                <th scope="row">{{ $article->id }}</th>
                <td>{{ $article->title }}</td>
                <td>{{ $article->slug }}</td>
                <td>{{ $article->subtitle }}</td>
                <td>
                    <a class="btn btn-primary" href="/admin/news/delete/{{ $article->id }}">Supprimer</a>
                    <a class="btn btn-primary" href="/admin/news/edit/{{ $article->id }}">Modifier</a>
                    <a class="btn btn-default" href="/admin/news/ban/{{ $article->id }}">Bannir</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection