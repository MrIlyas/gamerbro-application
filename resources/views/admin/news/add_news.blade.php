@extends("admin.templates.template_admin")

@section("content_admin")
    <form class="form-horizontal">
        <fieldset>

            <!-- Form Name -->
            <legend>Ajouter une actualité</legend>

            <!-- Text input-->
            <div class="control-group">
                <label class="control-label" for="Titre">Titre</label>
                <div class="controls">
                    <input id="Titre" name="Titre" type="text" placeholder="C'est ici que vous placerez votre actualité" class="input-xlarge" required="">
                    <p class="help-block">help</p>
                </div>
            </div>

            <!-- File Button -->
            <div class="control-group">
                <label class="control-label" for="img_news">Image à la une</label>
                <div class="controls">
                    <input id="img_news" name="img_news" class="input-file" type="file">
                </div>
            </div>

            <!-- Search input-->
            <div class="control-group">
                <label class="control-label" for="Jeux">Jeux</label>
                <div class="controls">
                    <input id="Jeux" name="Jeux" type="text" placeholder="Jeux à rechercher" class="input-xlarge search-query" required="">

                </div>
            </div>

            <!-- Textarea -->
            <div class="control-group">
                <label class="control-label" for="content">contenu</label>
                <div class="controls">
                    <textarea id="content" name="content">Laissez libre court a votre imagination</textarea>
                </div>
            </div>

            <!-- Select Basic -->
            <div class="control-group">
                <label class="control-label" for="status">Statut de l</label>
                <div class="controls">
                    <select id="status" name="status" class="input-xlarge">
                        <option>brouillon</option>
                        <option>en correction</option>
                        <option>prêt à être publié</option>
                        <option>publié</option>
                    </select>
                </div>
            </div>

            <!-- Multiple Radios -->
            <div class="control-group">
                <label class="control-label" for="radios">Multiple Radios</label>
                <div class="controls">
                    <label class="radio" for="radios-0">
                        <input type="radio" name="radios" id="radios-0" value="Article" checked="checked">
                        Article
                    </label>
                    <label class="radio" for="radios-1">
                        <input type="radio" name="radios" id="radios-1" value="Chronique">
                        Chronique
                    </label>
                    <label class="radio" for="radios-2">
                        <input type="radio" name="radios" id="radios-2" value="Vidéo">
                        Vidéo
                    </label>
                </div>
            </div>

            <!-- Button (Double) -->
            <div class="control-group">
                <label class="control-label" for="validation_btn">Double Button</label>
                <div class="controls">
                    <button id="validation_btn" name="validation_btn" class="btn btn-success">Valider</button>
                    <button id="reinit_btn" name="reinit_btn" class="btn btn-primary">Réinitialiser</button>
                </div>
            </div>

        </fieldset>
    </form>
@endsection