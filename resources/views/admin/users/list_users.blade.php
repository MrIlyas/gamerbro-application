@extends("admin.templates.template_admin")

@section("content_admin")
<div class="table-responsive">
    <h1>Liste des utilisateurs de <strong>Gamerbro.fr</strong></h1><br />
    <div class="collapse" id="collapse">
            Mouais je ferais pas ca a ta place !
    </div>
    <a class="btn btn-primary col-lg-12 btn-lg" href="/admin/users/create">Ajouter un nouveau membre manuellement</a><br /><br /><br />
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Pseudo</th>
            <th>Email</th>
            <th>Level</th>
            <th width="300px;">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->level }}</td>
                <td>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse">Supprimer</button>
                    <a class="btn btn-primary" href="/admin/users/edit/{{ $user->id }}">Modifier</a>
                    <a class="btn btn-default" href="/admin/users/ban/{{ $user->id }}">Bannir</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection