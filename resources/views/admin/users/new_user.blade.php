@extends("admin.templates.template_admin")

@section("content_admin")
    <h1>Enregistrer un nouvel utilisateur</h1>
    <form class="form col-md-center-block" method="POST" action="/auth/register">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" class="form-control input-lg" name="name" placeholder="Votre pseudonyme" value="{{ old('name') }}">
        </div>
        <div class="form-group">
            <input type="email" class="form-control input-lg" name="email" placeholder="Votre email" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <input type="password" class="form-control input-lg" name="password"  placeholder="Votre mot de passe">
        </div>

        <div class="form-group">
            <input type="password" class="form-control input-lg" name="password_confirmation"  placeholder="Confirmez votre mot de passe">
        </div>
        <div class="form-group">
                <button class="btn btn-primary btn-lg btn-block" type="submit">L'enregistrer</button>
        </div>
    </form>
@endsection