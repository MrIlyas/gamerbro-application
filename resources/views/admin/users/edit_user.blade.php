@extends("admin.templates.template_admin")

@section("content_admin")
   <h1>Modifier l'utilisateur <strong>{{$user->name}}</strong></h1>
    <form class="form col-md-center-block" method="POST" action="/auth/register">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" class="form-control input-lg" name="name" placeholder="Votre pseudonyme" value="{{ $user->name }}">
        </div>

        <div class="form-group">
            <input type="email" class="form-control input-lg" name="email" placeholder="Votre email" value="{{ $user->email  }}">
        </div>

        <div class="form-group">
            <input type="text" class="form-control input-lg" name="level" value="{{$user->password}}">
        </div>
        <div class="form-group">
            <input type="text" class="form-control input-lg" name="level" value="{{$user->level}}">
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-lg btn-block" type="submit">L'enregistrer</button>
        </div>
        <div class="form-group">
            <a class="btn btn-default btn-lg btn-block" href="/admin/users">Revenir à la liste</a>
        </div>
    </form>
@endsection