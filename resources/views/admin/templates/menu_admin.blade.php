<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li>
            <a href="/admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
        </li>
        <li>
            <a href="/admin/news"><i class="fa fa-fw fa-dashboard"></i> Liste d'actualités</a>
        </li>
        @if(Auth::user()->level >= 1)
            <li class="nav-divider"></li>
            <li>
                <a href="/admin/users"><i class="fa fa-fw fa-bar-chart-o"></i> Liste utilisateur</a>
            </li>
        @endif
    </ul>
</div>